/* TODO:
    Improve test coverage.
    Maybe -a for asm, -f for object file, -e for executable. -afe to get all three, like pacman.
    Add llvmir to the targets available.
    Improve internal error handling, or at least make sure it's good.
    ??
*/

use std::fmt;

pub const READ_BYTE: char = ',';
pub const PRINT_BYTE: char = '.';
pub const INC_BYTE: char = '+';
pub const DEC_BYTE: char = '-';
pub const INC_PTR: char = '>';
pub const DEC_PTR: char = '<';
pub const LOOP_OPEN: char = '[';
pub const LOOP_CLOSE: char = ']';

// This exists to be used e.g. TOKENS.contains(char) to check if a given char is a valid bf token
const TOKENS: &[char; 8] = &[
    READ_BYTE, PRINT_BYTE, INC_BYTE, DEC_BYTE, INC_PTR, DEC_PTR, LOOP_OPEN, LOOP_CLOSE,
];

// Size of the bf memory space in bytes
pub const MEM_LEN: usize = 30000;

// There are two reasons a bf program might be invalid:
//      1) The cell pointer value exceeds the bounds of the memory space.
//      2) There exists an unmatched bracket.
// Only the latter can be easily caught at compile time. The fields represent the line and column where the error occured, respectively.
#[derive(Debug)]
pub enum Fail {
    UnmatchedBrackets {
        open_brackets: Vec<(usize, usize)>,
        close_brackets: Vec<(usize, usize)>,
    },
}

impl fmt::Display for Fail {
    fn fmt(&self, fmtr: &mut fmt::Formatter) -> fmt::Result {
        fn maybe_plural(n: usize) -> &'static str {
            if n > 1 {
                "locations"
            } else {
                "location"
            }
        }
        match self {
            Self::UnmatchedBrackets {
                open_brackets,
                close_brackets,
            } => {
                let mut output = String::new();
                if !open_brackets.is_empty() {
                    output.push_str(&format!(
                        "Unmatched opening bracket at the following {} (line, column):\n{:?}",
                        maybe_plural(open_brackets.len()),
                        open_brackets
                    ));
                } else {
                    output.push_str(&format!(
                        "Unmatched closing bracket at the following {} (line, column):\n{:?}",
                        maybe_plural(close_brackets.len()),
                        close_brackets
                    ));
                }
                write!(fmtr, "{}", output)
            }
        }
    }
}

// Returns Ok(&str) of the passed str if all brackets are matched, otherwise Err() containing the coordinates of an unmatched bracket in the passed &str as a tuple of form (line, column).
pub fn good_brackets(s: &str) -> Result<&str, Fail> {
    let mut open_stack: Vec<(usize, usize)> = vec![];
    let mut close_stack: Vec<(usize, usize)> = vec![];
    for (lq, l) in s.lines().enumerate() {
        for (cq, c) in l.chars().enumerate() {
            if c == LOOP_OPEN {
                open_stack.push((lq + 1, cq + 1));
            } else if c == LOOP_CLOSE {
                if !open_stack.is_empty() {
                    open_stack.pop();
                } else {
                    close_stack.push((lq + 1, cq + 1));
                }
            }
        }
    }
    if open_stack.is_empty() && close_stack.is_empty() {
        Ok(s)
    } else {
        Err(Fail::UnmatchedBrackets {
            open_brackets: open_stack,
            close_brackets: close_stack,
        })
    }
}

// Verifies bracket matching and removes comment characters, i.e. any character for which TOKENS.contains() is not true.
// This helps to simplify the logic later on.
fn clean(src: &str) -> Result<Vec<char>, Fail> {
    Ok(good_brackets(src)?
        .chars()
        .filter(|c| TOKENS.contains(c))
        .collect())
}

pub mod clean_struct {
    // Do NOT build one of these with struct literal syntax. If you circumvent the module restictions and do it anyways, that's on you.
    // Just wraps Vec<char>. Provides an easy interface for taking verifiably* clean() bf source.
    #[derive(Debug)]
    pub struct CleanSrc {
        tokens: Vec<char>,
    }

    impl CleanSrc {
        pub fn from(s: &str) -> Result<Self, crate::Fail> {
            match crate::clean(s) {
                Ok(val) => Ok(Self { tokens: val }),
                Err(e) => Err(e),
            }
        }
        pub fn tokens(&self) -> &Vec<char> {
            &self.tokens
        }
    }
}

use clean_struct::*;

// Returns the character index of the '[' matching the ']' at character index n.
pub fn match_backward(instructions: &CleanSrc, n: usize) -> usize {
    let mut result: usize = 0;
    let mut depth = 0_i32;
    for q in (0..=n).rev() {
        if instructions.tokens()[q] == LOOP_CLOSE {
            depth -= 1;
        } else if instructions.tokens()[q] == LOOP_OPEN {
            depth += 1;
        }
        if depth == 0 {
            result = q;
            break;
        }
    }
    result
}

pub fn match_forward(instructions: &CleanSrc, n: usize) -> usize {
    let mut depth = 0;
    let mut result: usize = 0;
    for q in n..instructions.tokens().len() {
        if instructions.tokens()[q] == LOOP_OPEN {
            depth += 1;
        } else if instructions.tokens()[q] == LOOP_CLOSE {
            depth -= 1;
        }
        if depth == 0 {
            result = q;
            break;
        }
    }
    result
}

// Syscall values
const SYS_EXIT: u8 = 60;
const SUCCESS: u8 = 0;

const SYS_WRITE: u8 = 1;
const STDOUT: u8 = 1;

const SYS_READ: u8 = 0;
const STDIN: u8 = 0;

// Set of supported target platforms for RunMode Compile. Remember to update Target::from_triple when adding a new Target variant.
#[derive(Debug, Clone, Copy)]
pub enum Target {
    Amd64Linux,
}

impl Target {
    pub fn as_triple(&self) -> &'static str {
        match self {
            Self::Amd64Linux => "x86_64-unknown-linux",
        }
    }
    pub fn from_triple(s: &str) -> Result<Self, &'static str> {
        let parts: Vec<&str> = s.split('-').collect();
        if parts.len() < 3 {
            return Err("Failed to parse unsupported target triple");
        }
        let double = format!("{}-{}", parts[0], parts[2]);
        match double.as_str() {
            "x86_64-linux" => Ok(Self::Amd64Linux),
            "amd64-linux" => Ok(Self::Amd64Linux),
            "x64-linux" => Ok(Self::Amd64Linux),
            _ => Err("Failed to parse unsupported target triple"),
        }
    }
}

// An instruction format that is easy to parse into asm. e.g. IncPtr {4} -> add r15, 4.
// Repeated symbols are condensed into a single variant. Bracket matches are encoded.
#[derive(Debug)]
pub enum Iri {
    ReadByte,
    PrintByte,
    IncByte { times: usize },
    DecByte { times: usize },
    IncPtr { times: usize },
    DecPtr { times: usize },
    LoopOpen { i: usize, goto: usize },
    LoopClose { i: usize, goto: usize },
}

impl Iri {
    fn amd64_linux(&self) -> String {
        match self {
            Self::ReadByte => format!(
                "\tmov rax, {}\n\tmov rdi, {}\n\tmov rsi, r15\n\tmov rdx, 1\n\tsyscall\n",
                SYS_READ, STDIN
            ),
            Self::PrintByte => format!(
                "\tmov rax, {}\n\tmov rdi, {}\n\tmov rsi, r15\n\tmov rdx, 1\n\tsyscall\n",
                SYS_WRITE, STDOUT
            ),
            Self::IncByte { times } => format!("\taddb [r15], {}\n", times),
            Self::DecByte { times } => format!("\tsubb [r15], {}\n", times),
            Self::IncPtr { times } => format!("\tadd r15, {}\n", times),
            Self::DecPtr { times } => format!("\tsub r15, {}\n", times),
            Self::LoopOpen { i, goto } => {
                format!("cmpb [r15], 0\nje LOOP_CLOSE_{}\nLOOP_OPEN_{}:\n", goto, i)
            }
            Self::LoopClose { i, goto } => {
                format!("cmpb [r15], 0\njne LOOP_OPEN_{}\nLOOP_CLOSE_{}:\n", goto, i)
            }
        }
    }
    fn to_asm(&self, t: &Target) -> String {
        match t {
            Target::Amd64Linux => self.amd64_linux(),
        }
    }
}

fn count_same_consecutive(instructions: &CleanSrc, n: usize) -> usize {
    let mut c = 0;
    for q in n..instructions.tokens().len() {
        if instructions.tokens()[q] == instructions.tokens()[n] {
            c += 1;
        } else {
            break;
        }
    }
    c
}

pub fn gen_ir(src: &CleanSrc) -> Vec<Iri> {
    let mut i: usize = 0;
    let mut iris: Vec<Iri> = vec![];

    while i < src.tokens().len() {
        iris.push(match src.tokens()[i] {
            READ_BYTE => {
                i += 1;
                Iri::ReadByte
            }
            PRINT_BYTE => {
                i += 1;
                Iri::PrintByte
            }
            INC_BYTE => {
                let num_same_consecutive = count_same_consecutive(&src, i);
                i += num_same_consecutive;
                Iri::IncByte { times: num_same_consecutive }
            }
            DEC_BYTE => {
                let num_same_consecutive = count_same_consecutive(&src, i);
                i += num_same_consecutive;
                Iri::DecByte { times: num_same_consecutive }
            }
            INC_PTR => {
                let num_same_consecutive = count_same_consecutive(&src, i);
                i += num_same_consecutive;
                Iri::IncPtr { times: num_same_consecutive }
            }
            DEC_PTR => {
                let num_same_consecutive = count_same_consecutive(&src, i);
                i += num_same_consecutive;
                Iri::DecPtr { times: num_same_consecutive }
            }
            LOOP_OPEN => {
                i += 1;
                Iri::LoopOpen {
                    i: i - 1,
                    goto: match_forward(&src, i - 1)
                }
            }
            LOOP_CLOSE => {
                i += 1;
                Iri::LoopClose {
                    i: i - 1,
                    goto: match_backward(&src, i - 1),
                }
            }
            _ => unreachable!(
                "Illegal char encountered in function gen_ir(src: &CleanSrc) -> Vec<IRI>. To ensure struct integrity, CleanSrc instances should be immutable and instantiated only via CleanSrc::from(). The CleanSrc passed was mutated or instantiated literally."
            ),
        });
    }
    iris
}

// Returns boilerplate to be appended to the top of final asm source output.
fn asm_head(t: &Target) -> String {
    match t {
        Target::Amd64Linux => format!(".intel_syntax noprefix\n.data\n.bss\n.lcomm MEM, {}\n.text\n.global _start\n_start:\n\nmov r15, offset MEM\n\n", MEM_LEN),
    }
}

// Returns boilerplate to be appended to the bottom of final asm source output.
fn asm_tail(t: &Target) -> String {
    match t {
        Target::Amd64Linux => format!("mov rax, {}\nmov rdi, {}\nsyscall\n", SYS_EXIT, SUCCESS),
    }
}

// Appends boilerplate and converts IRI's to asm. This function returns a complete bf program as a String of asm instructions.
pub fn compile(iris: &[Iri], t: &Target) -> String {
    let mut asm_source = String::new();

    asm_source.push_str(&asm_head(t));
    for i in iris {
        asm_source.push_str(&format!("{}\n", i.to_asm(t)));
    }
    asm_source.push_str(&asm_tail(t));

    asm_source
}

#[cfg(test)]
mod tests {
    use super::*;

    const HW_SRC: &'static str = "++++++++++\nThis is a loop![>+++++++>++++++++++>+++>+<<<<-]\nTime to\n correct ou\nr precision!>++.>+.+++++++..+++.>++.<<+++++++++++++++.>.+++.------.--------.>+.>.";

    #[test]
    fn test_gen_ir() {
        let src = CleanSrc::from(HW_SRC).unwrap();
        for q in 0..43 {
            println!("{}", gen_ir(&src)[q].to_asm(&Target::Amd64Linux));
        }
    }

    #[test]
    fn test_compile() {
        let src = CleanSrc::from(HW_SRC).unwrap();
        let iris = gen_ir(&src);
        compile(&iris, &Target::Amd64Linux);
    }
}
