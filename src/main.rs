/* TODO:
    1. Refactor interface/main.rs. Really think it over this time.
*/

use mbfc::{clean_struct::*, *};
#[macro_use]
extern crate clap;
use clap::{App, Arg};
use guess_host_triple::guess_host_triple;
use std::{fs, path::PathBuf, process::Command};

// All of the commented out stuff below is kept as a reference for when I reimplment the interpreter in the future. It's not meant to be uncommented.

// use std::{
//     fs, io,
//     io::{Bytes, Read, Write},
//     path::PathBuf,
//     process::Command,
// };

// #[derive(Debug)]
// enum Fail {
//     PointerOutOfBounds(usize, usize),
// }

// TODO: Make it so that OOB pointer Fail variant shows the line/column indices, not the token index.
// pub fn run_interpret<I: Read, O: Write>(
//     mut input: Bytes<I>,
//     output: &mut O,
//     instructions: CleanSrc,
// ) -> Result<(), Fail> {
//     let mut mem = [0_u8; MEM_LEN];
//     let mut cell_ptr: usize = 0;
//     let mut token_ptr: usize = 0;

//     while token_ptr < (&instructions.tokens).len() {
//         match instructions.tokens[token_ptr] {
//             READ_BYTE => {
//                 output.flush().unwrap();
//                 match input.next() {
//                     Some(Ok(b)) => {
//                         mem[cell_ptr] = b as u8;
//                     }
//                     Some(Err(e)) => {
//                         panic!("{}",e)
//                     }
//                     None => {
//                         mem[cell_ptr] = 0;
//                     }
//                 }
//                 token_ptr += 1;
//             }
//             PRINT_BYTE => {
//                 output.write_all(&[mem[cell_ptr] as u8]).unwrap();
//                 token_ptr += 1;
//             }
//             INC_BYTE => {
//                 mem[cell_ptr] += 1;
//                 token_ptr += 1;
//             }
//             DEC_BYTE => {
//                 mem[cell_ptr] -= 1;
//                 token_ptr += 1;
//             }
//             INC_PTR => {
//                 if cell_ptr + 1 >= MEM_LEN {
//                     return Err(Fail::PointerOutOfBounds(0, token_ptr));
//                 }
//                 cell_ptr += 1;
//                 token_ptr += 1;
//             }
//             DEC_PTR => {
//                 cell_ptr -= 1;
//                 token_ptr += 1;
//             }
//             LOOP_OPEN => {
//                 if mem[cell_ptr] == 0 {
//                     token_ptr = match_forward(&instructions, token_ptr) + 1;
//                 } else {
//                     token_ptr += 1;
//                 }
//             }
//             LOOP_CLOSE => {
//                 token_ptr = match_backward(&instructions, token_ptr);
//             }
//             _ => panic!("Failed to read illegal character"),
//         }
//     }
//     Ok(())
// }

enum RunMode {
    Compile,
    Interpret,
    Asm,
}

fn main() -> std::io::Result<()> {
    let matches = App::new("mbfc")
        .version(crate_version!())
        .about("This program is a simple, WIP brainfuck compiler and interpreter. Compiling has runtime dependiencies on gas and ld.")
        .arg(Arg::with_name("interpret")
            .short("i")
            .long("interpret")
            .help("Runs your bf program in an interpreted fashion (not currently working)")
            .conflicts_with_all(&["triple", "messy", "asm"]))
        .arg(Arg::with_name("source")
            .help("Sets the brainfuck source file to use")
            .required(true))
        .arg(Arg::with_name("triple")
            .help("Sets target platform for compilation; takes a triple, e.g. \"x86_64-unknown-linux\"")
            .takes_value(true)
            .long("target")
            .short("t"))
        .arg(Arg::with_name("output")
            .help("Sets path of output file")
            .takes_value(true)
            .long("output")
            .short("o"))
        .arg(Arg::with_name("messy")
            .help("Leaves behind intermediate files after compilation")
            .long("messy")
            .short("m"))
        .arg(Arg::with_name("asm")
            .help("Outputs asm; that's it")
            .long("asm")
            .conflicts_with_all(&["messy", "output"]))
        .get_matches();

    let source_path = matches.value_of("source").unwrap();
    let run_mode = if matches.is_present("interpret") {
        RunMode::Interpret
    } else if matches.is_present("asm") {
        RunMode::Asm
    } else {
        RunMode::Compile
    };

    //add an option

    // Do not change the following line to "match &run_mode {". Consuming the enum is a forcing function for good arm design.
    match run_mode {
        RunMode::Compile => {
            let target = Target::from_triple(matches.value_of("triple").unwrap_or_else(|| {
                guess_host_triple().expect("Failed to guess target; please provide a triple")
            }))
            .unwrap();

            let source_head = PathBuf::from(source_path)
                .file_stem()
                .expect("Failed to extract file stem from provided source path")
                .to_str()
                .expect("Provided source path is not valid unicode")
                .to_string();

            let output_path = matches.value_of("output").unwrap_or(&source_head);
            let source = fs::read_to_string(source_path)?;

            let ir = if !source.is_empty() {
                gen_ir(&CleanSrc::from(&source).unwrap_or_else(|e| {
                    eprintln!("{}", e);
                    std::process::exit(1);
                }))
            } else {
                panic!("Source file is empty")
            };

            let asm_path = format!("{}.asm", &output_path);
            let object_path = format!("{}.o", &output_path);

            fs::write(&asm_path, compile(&ir, &target))?;

            Command::new("/usr/bin/env")
                .arg("as")
                .arg(&asm_path)
                .arg("-o")
                .arg(&object_path)
                .spawn()
                .expect("Failed to assemble generated asm with as")
                .wait()?;

            Command::new("/usr/bin/env")
                .arg("ld")
                .arg(&object_path)
                .arg("-o")
                .arg(&output_path)
                .spawn()
                .expect("Failed to link generated object file with ld")
                .wait()?;

            if !matches.is_present("messy") {
                fs::remove_file(&object_path)?;
                fs::remove_file(&asm_path)?;
            }
        }
        RunMode::Interpret => {
            // let stdin = io::stdin();
            // let input = stdin.lock().bytes();
            // let stdout = io::stdout();
            // let mut output = stdout.lock();
            // run_interpret(
            //     input,
            //     &mut output,
            //     CleanSrc::from(&fs::read_to_string(source_path)?).unwrap_or_else(|e| panic!("{}", e)),
            // )
            // .unwrap();
            println!("Interpreting doesn't work right now; I'm reimplementing it to improve the internal structure of mbfc.");
            println!("Don't worry! It will be back soon. In the meantime, compiling is working better than ever.");
        }
        RunMode::Asm => {
            let target = Target::from_triple(matches.value_of("triple").unwrap_or_else(|| {
                guess_host_triple().expect("Failed to guess target; please provide a triple")
            }))
            .unwrap();

            let source_head = PathBuf::from(source_path)
                .file_stem()
                .expect("Failed to extract file stem from provided source path")
                .to_str()
                .expect("Provided source path is not valid unicode")
                .to_string();

            let output_path = matches.value_of("output").unwrap_or(&source_head);
            let source = fs::read_to_string(source_path)?;

            let ir = if !source.is_empty() {
                gen_ir(&CleanSrc::from(&source).unwrap())
            } else {
                panic!("Source file is empty")
            };

            let asm_path = format!("{}.asm", &output_path);

            fs::write(&asm_path, compile(&ir, &target))?;
        }
    }
    Ok(())
}
